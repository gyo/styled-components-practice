import styled from "styled-components";
import React from "react";

const Input = styled.input`
  padding: 0.5em;
  margin: 0.5em;
  color: palevioletred;
  background: papayawhip;
  border: none;
  border-radius: 3px;
`;

const Form: React.FC = () => {
  const inputRef = React.createRef<HTMLInputElement>();
  return (
    <Input
      ref={inputRef}
      placeholder="Hover to focus!"
      onMouseEnter={() => {
        if (inputRef.current == null) {
          return;
        }
        inputRef.current.focus();
      }}
    />
  );
};

export default Form;
