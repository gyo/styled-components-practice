import styled from "styled-components";
import React from "react";

const Box = styled.div({
  background: "palevioletred",
  height: "50px",
  width: "50px"
});

interface IProps {
  background: string;
}

const PropsBox = styled.div((props: IProps) => ({
  background: props.background,
  height: "50px",
  width: "50px"
}));

const StyleObjects: React.FC = () => (
  <>
    <Box />
    <PropsBox background="red" />
  </>
);

export default StyleObjects;
