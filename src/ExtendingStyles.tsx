import styled from "styled-components";
import React from "react";

const Button = styled.button`
  color: palevioletred;
  background: white;
  font-size: 1em;
  margin: 1em;
  padding: 0.25em 1em;
  border: 2px solid palevioletred;
  border-radius: 3px;
`;

const TomatoButton = styled(Button)`
  color: tomato;
  border-color: tomato;
`;

const ExtendingStyles: React.FC = () => (
  <>
    <Button>Normal</Button>
    <TomatoButton>Tomato</TomatoButton>
  </>
);

export default ExtendingStyles;
