import styled, { createGlobalStyle } from "styled-components";
import React from "react";
import "./App.css";
import AdaptingBasedOnProps from "./AdaptingBasedOnProps";
import Animations from "./Animations";
import AttachingAdditionalProps from "./AttachingAdditionalProps";
import ComingFromCSS from "./ComingFromCSS";
import ExtendingStyles from "./ExtendingStyles";
import GettingStarted from "./GettingStarted";
import MediaTemplates from "./MediaTemplates";
import MyComponent from "./MyComponent";
import PassedProps from "./PassedProps";
import ReferringToOtherComponents from "./ReferringToOtherComponents";
import Refs from "./Refs";
import Reset from "./Reset";
import Security from "./Security";
import StyleObjects from "./StyleObjects";
import StylingAnyComponents from "./StylingAnyComponents";
import StylingNormalReactComponents from "./StylingNormalReactComponents";
import Theming from "./Theming";

const GlobalStyle = createGlobalStyle`
  body {
    background: lightcyan;
  }
`;

const OverrideMyComponent = styled(MyComponent)`
  color: red;
  span {
    color: blue;
  }
`;

const App: React.FC = () => {
  return (
    <>
      <GlobalStyle />

      <MyComponent />
      <MyComponent width={120} />
      <OverrideMyComponent />
      <GettingStarted />
      <AdaptingBasedOnProps />
      <ExtendingStyles />
      <StylingAnyComponents />
      <PassedProps />
      <ComingFromCSS />
      <AttachingAdditionalProps />
      <Animations />
      <Theming />
      <Refs />
      <Security />
      <div className="red">.red</div>
      <div>
        <div>
          <div>div > div > div</div>
        </div>
      </div>
      <StylingNormalReactComponents className="red" />
      <MediaTemplates />
      <ReferringToOtherComponents />
      <StyleObjects />
      <div>
        <p>ほげ</p>
        <p>ほげ</p>
        <p>ほげ</p>
      </div>
      <Reset>
        <p>ほげ</p>
        <p>ほげ</p>
        <p>ほげ</p>
      </Reset>
      <div style={{ height: "500px" }} />
    </>
  );
};

export default App;
