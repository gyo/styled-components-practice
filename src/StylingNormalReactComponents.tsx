import styled from "styled-components";
import React from "react";

interface IProps {
  className?: string;
}

const GreenDiv = styled.div`
  background-color: green;
`;

const NormalDiv = styled.div`
  display: block;
`;

const MyComponent: React.FC<IProps> = ({ className }) => {
  return (
    <>
      <div className={className}>.red</div>
      <div>
        <div>
          <div>div > div > div</div>
        </div>
      </div>
      <NormalDiv>NormalDiv</NormalDiv>
      <NormalDiv className="red-bg">NormalDiv</NormalDiv>
      <GreenDiv>GreenDiv</GreenDiv>
      <GreenDiv className="red-bg">GreenDiv</GreenDiv>
    </>
  );
};

export default MyComponent;
