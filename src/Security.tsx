import React from "react";

const Form: React.FC = () => {
  return (
    <div>
      Using user input as styles can lead to any CSS being evaluated in the
      user's browser that an attacker can place in your application. It's not
      very well supported across browsers yet, so we recommend using the{" "}
      <a
        href="https://github.com/mathiasbynens/CSS.escape"
        target="_blank"
        rel="noopener noreferrer"
      >
        polyfill by Mathias Bynens
      </a>{" "}
      in your app.
    </div>
  );
};

export default Form;
