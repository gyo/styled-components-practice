/**
 * 以下の記事を参考に
 *
 * styled-componentsによる抽象コンポーネント作成のすゝめ - Mercari Engineering Blog
 * https://tech.mercari.com/entry/2019/06/03/120000
 *
 */

import styled from "styled-components";

/**
 * こういう書き方だと、堅牢性に欠けるかもしれない
 *
 * > そもそも、グローバル CSSへ依存したコンポーネントの実装は堅牢な設計とは言えません。
 */
const Reset = styled.div`
  * {
    margin: 0;
    padding: 0;
  }
`;

export default Reset;
