import styled from "styled-components";
import React from "react";

interface IAttrs {
  size?: number;
}

const Input = styled.input.attrs(({ size }: IAttrs) => ({
  type: "password",
  margin: size != null ? `${size}em` : "1em",
  padding: size != null ? `${size}em` : "1em"
}))`
  color: palevioletred;
  font-size: 1em;
  border: 2px solid palevioletred;
  border-radius: 3px;

  /* here we use the dynamically computed props */
  margin: ${props => props.margin};
  padding: ${props => props.padding};
`;

const AttachingAdditionalProps: React.FC = () => (
  <>
    <div>
      <Input placeholder="A small text input" />
    </div>
    <div>
      <Input placeholder="A bigger text input" size={2} />
    </div>
  </>
);

export default AttachingAdditionalProps;
