import styled, { createGlobalStyle } from "styled-components";
import React from "react";

const StyledCounter = styled.div`
  padding: 1em;
  background: wheat;
`;

const StyledParagraph = styled.p`
  padding: 1em;
`;

const StyledButton = styled.button`
  padding: 1em;
`;

const Counter: React.FC = () => {
  const [count, setCount] = React.useState(0);

  return (
    <StyledCounter>
      <StyledParagraph>{`${count}`}</StyledParagraph>
      <StyledButton onClick={() => setCount(count + 1)}>increment</StyledButton>
      <StyledButton onClick={() => setCount(count - 1)}>decrement</StyledButton>
    </StyledCounter>
  );
};

const Thing = styled.div.attrs({ tabIndex: 0 })`
  padding: 1em;
  color: blue;

  :hover {
    color: red;
  }

  & + & {
    background: lime;
  }

  &.modifier {
    background: orange;
  }

  .sibling + & {
    border: 1px solid brown;
  }

  .parent & {
    color: pink;
  }

  ::before {
    content: "🚀";
  }

  // If you put selectors in without the ampersand, they will refer to children of the component.
  .child {
    padding: 0.5em;
    background: aliceblue;
    color: midnightblue;
  }
`;

// important のようなもの？
const SpecificityThing = styled.div`
  && {
    color: blue;
  }
`;

const GlobalStyle = createGlobalStyle`
  div${SpecificityThing} {
    color: green;
  }
`;

const ComingFromCSS: React.FC = () => (
  <>
    <GlobalStyle />
    <Counter />
    <Thing>Hello world!</Thing>
    <Thing>Hello world!</Thing>
    <Thing className="modifier">With Modifier</Thing>
    <div className="sibling">Sibling</div>
    <Thing>Hello world!</Thing>
    <div className="parent">
      <Thing>Hello world!</Thing>
    </div>
    <Thing>
      <div className="child">Child</div>
      <div className="child">Child</div>
      <div className="child">Child</div>
    </Thing>
    <SpecificityThing>Hello world!</SpecificityThing>
  </>
);

export default ComingFromCSS;
