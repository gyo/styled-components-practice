import styled from "styled-components";
import React from "react";

interface IProps {
  className?: string;
}

const Link: React.FC<IProps> = ({ className, children }) => (
  <a className={className} href="/">{children}</a>
);

const StyledLink = styled(Link)`
  color: palevioletred;
  font-weight: bold;
`;

const StylingAnyComponents: React.FC = () => (
  <>
    <div>
      <Link>Unstyled</Link>
    </div>
    <div>
      <StyledLink>Styled</StyledLink>
    </div>
  </>
);

export default StylingAnyComponents;
