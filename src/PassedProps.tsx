import styled from "styled-components";
import React from "react";

interface IProps {
  inputColor?: string;
}

const Input = styled.input`
  padding: 0.5em;
  margin: 0.5em;
  color: ${(props: IProps) =>
    props.inputColor != null ? props.inputColor : "palevioletred"};
  background: papayawhip;
  border: none;
  border-radius: 3px;
`;

const PassedProps: React.FC = () => (
  <>
    <Input type="text" defaultValue="@probablyup" />
    <Input type="text" defaultValue="@geelen" inputColor="rebeccapurple" />
  </>
);

export default PassedProps;
