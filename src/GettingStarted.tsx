import styled from "styled-components";
import React from "react";

export const Title = styled.h1`
  font-size: 1.5em;
  text-align: center;
  color: palevioletred;
`;

export const Wrapper = styled.section`
  padding: 4em;
  background: papayawhip;
`;

const GettingStarted: React.FC = () => (
  <Wrapper>
    <Title>Hello World!</Title>
  </Wrapper>
);

export default GettingStarted;
