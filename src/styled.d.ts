// import original module declarations
import "styled-components";

// and extend them!
declare module "styled-components" {
  export interface DefaultTheme {
    fg?: string;
    bg?: string;
    fz?: number;
  }
}
