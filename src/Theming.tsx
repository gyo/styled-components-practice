import styled, { DefaultTheme, ThemeProvider } from "styled-components";
import React from "react";

const Button = styled.button`
  font-size: 1em;
  margin: 1em;
  padding: 0.25em 1em;
  border-radius: 3px;

  color: ${props => props.theme.fg};
  background: ${props => props.theme.bg};
`;

const theme: DefaultTheme = {
  fg: "mediumseagreen",
  bg: "white"
};

const invertTheme = ({ fg, bg }: DefaultTheme) => ({
  fg: bg,
  bg: fg
});

const Theming: React.FC = () => (
  <ThemeProvider theme={theme}>
    <>
      <Button>Themed</Button>
      <Button theme={{ fg: "royalblue", bg: "white" }}>Ad hoc theme</Button>
      <ThemeProvider theme={invertTheme}>
        <>
          <Button>Inverted Theme</Button>
          <Button theme={{ fg: "darkorange", bg: "white" }}>
            Ad hoc theme
          </Button>
        </>
      </ThemeProvider>
    </>
  </ThemeProvider>
);

export default Theming;
