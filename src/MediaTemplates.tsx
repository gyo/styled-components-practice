import styled, { css } from "styled-components";
import React from "react";

const Content = styled.div`
  background: papayawhip;
  height: 3em;
  width: 3em;

  @media (max-width: 700px) {
    background: palevioletred;
  }
`;

const sizes = {
  desktop: 992,
  tablet: 768,
  phone: 576
};

const media = Object.keys(sizes).reduce(
  //@ts-ignore
  (acc, label: "desktop" | "tablet" | "phone") => {
    const size = sizes[label];

    //@ts-ignore
    acc[label] = (...args) => css`
      @media (max-width: ${size / 16}em) {
        //@ts-ignore
        ${css(...args)}
      }
    `;

    return acc;
  },
  {}
);

const Content2 = styled.div`
  height: 3em;
  width: 3em;
  background: papayawhip;

  /* Now we have our methods on media and can use them instead of raw queries */
  ${media.desktop`background: dodgerblue;`}
  ${media.tablet`background: mediumseagreen;`}
  ${media.phone`background: palevioletred;`}
`;

const MediaTemplates: React.FC = () => (
  <>
    <Content />
    <Content2 />
  </>
);

export default MediaTemplates;
