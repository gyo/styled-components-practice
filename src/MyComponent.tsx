import styled from "styled-components";
import React from "react";

interface IMyComponentProps {
  className?: string;
  width?: number;
}

const NonStyledComponent: React.FC<IMyComponentProps> = ({ className }) => (
  <div className={className}>
    Hoge<span>Fuga</span>
  </div>
);

const MyComponent = styled(NonStyledComponent)`
  width: ${(props: IMyComponentProps) =>
    props.width != null ? `${props.width}px` : "auto"};
  color: palevioletred;
  font-weight: bold;
  background: gray;
  span {
    color: skyblue;
  }
  :hover {
    opacity: 0.5;
  }
  ::after {
    content: "pseudo";
  }
  @media (max-width: 700px) {
    background: black;
  }
`;

export default MyComponent;
