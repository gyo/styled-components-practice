import styled from "styled-components";
import React from "react";

interface IProps {
  primary?: boolean;
}

const Button = styled.button`
  background: ${(props: IProps) =>
    props.primary != null ? "palevioletred" : "white"};
  color: ${(props: IProps) =>
    props.primary != null ? "white" : "palevioletred"};
  font-size: 1em;
  margin: 1em;
  padding: 0.25em 1em;
  border: 2px solid palevioletred;
  border-radius: 3px;
`;

const AdaptingBasedOnProps: React.FC = () => (
  <>
    <Button>Normal</Button>
    <Button primary={true}>Primary</Button>
  </>
);

export default AdaptingBasedOnProps;
